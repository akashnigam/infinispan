# AWS setup - Infinispan Tutorial

## Setup Instance

### Setup ubuntu 16 instance

- SSH to instance

- Install java

```
sudo apt-get update
add-apt-repository ppa:webupd8team/java
sudo apt-get update
apt-get install oracle-java8-installer
```

- Update JAVA_HOME

```
vi /etc/environment
add -> JAVA_HOME="/usr/lib/jvm/java-8-oracle"
source /etc/environment
```

- Install apache maven

```
sudo apt-get install maven
```

- Download wildfly 12

```
cd /opt/
sudo wget http://download.jboss.org/wildfly/12.0.0.Final/wildfly-12.0.0.Final.tar.gz
```

- Extract

```
sudo tar -xzvf wildfly-12.0.0.Final.tar.gz
```

- Create symlink and remove the package

```
sudo ln -s wildfly-12.0.0.Final wildfly
```

- Clone repo

```
git clone <your repo>.git
```

- Maven build project

```
cd <your project>
mvn clean install
```

### Register wildfly as a service

- For simplicity, we will register wildfly for root user

```
cd /opt/
sudo chown -R root.root wildfly
sudo chown -R root.root wildfly-12.0.0.Final/
```
- Add JBOSS_HOME

```
sudo nano /etc/environment
add -> JBOSS_HOME="/opt/wildfly"
source /etc/environment
```

- Make changes to `wildfly.conf` for service registry

```
cd /opt/wildlfy/docs/contrib/scripts/init.d
sudo nano wildfly.conf
```

- Make following changes

```
	## Location of JDK      
	 JAVA_HOME="/usr/lib/jvm/java-8-oracle"
	
	## Location of WildFly
	 JBOSS_HOME="/opt/wildfly"
	
	## The username who should own the process.
	 JBOSS_USER=root
	
	## The mode WildFly should start, standalone or domain
	 JBOSS_MODE=standalone
	
	## Configuration for standalone mode
	 JBOSS_CONFIG=standalone.xml
	
	## The amount of time to wait for startup
	 STARTUP_WAIT=60
	
	## The amount of time to wait for shutdown
	 SHUTDOWN_WAIT=60
	
	## Location to keep the console log
	 JBOSS_CONSOLE_LOG="/var/log/wildfly/console.log"
```

- Copy required files

```
sudo cp wildfly.conf /etc/default/wildfly
sudo cp wildfly-init-debian.sh /etc/init.d/wildfly
sudo update-rc.d wildfly defaults
sudo systemctl daemon-reload
```

- Wildfly can now be accessed as `service wildfly start|stop|restart|status`

- Create symlik for logs

```
cd ~
ln -s /var/log/wildfly/console.log server.log
```

- Configure timezone for the app

```
sudo nano /opt/wildfly/bin/standalone.conf
```
and add this at the end

```
JAVA_OPTS="$JAVA_OPTS -Duser.timezone=IST -Djgroups.tcp.address=<private ip of your instance>"
```

- Check status

`sudo service wildfly status`

- Start wildfly

`sudo service widlfly start`

- Stop wildfly

`sudo service widlfly stop`

### Wildfly allow traffic from network

- Edit standalone.xml and change

```
<interface name="public">
    <inet-address value="${jboss.bind.address:127.0.0.1}"/>
</interface>
```
to
```
<interface name="public">
    <inet-address value="${jboss.bind.address:0.0.0.0}"/>
</interface>
```

### Assign an Elastic IP if you wish

### Assign a security group

- A security group will help in setting our application accessible from outside world
- Create a security group, opening port 8080 and assigning it to our EC2 instance.

## And Voila!, our application is reachable via browser!