# Project Setup

## Generate Project

mvn archetype:generate -DgroupId=com.infinispan -DartifactId=infinispan-tutorial -DarchetypeArtifactId=maven-archetype-webapp -DinteractiveMode=false


## Add POM dependencies

- Add `javaee-api`
```
	<dependency>
		<groupId>javax</groupId>
		<artifactId>javaee-api</artifactId>
		<version>7.0</version>
		<scope>provided</scope>
	</dependency>
```

- Add `javax.servlet-api`
```
	<dependency>
		<groupId>javax.servlet</groupId>
		<artifactId>javax.servlet-api</artifactId>
		<version>3.1.0</version>
		<scope>provided</scope>
	</dependency>
```

- Add `infinispan-core`
```
	<dependency>
      <groupId>org.infinispan</groupId>
      <artifactId>infinispan-core</artifactId>
      <version>9.1.0.Final</version>
    </dependency>
```

- Add `infinispan-cdi-embedded` for cdi support
```
	<dependency>
		<groupId>org.infinispan</groupId>
		<artifactId>infinispan-cdi-embedded</artifactId>
		<version>${infinispan.version}</version>
	</dependency>
```

- Add `native-s3-ping` for providing node discovery protocol for jgroups 4.0
```
	<dependency>
		<groupId>org.jgroups.aws.s3</groupId>
		<artifactId>native-s3-ping</artifactId>
		<version>0.9.3.Final</version>
	</dependency>
```

- Add `maven-compiler-plugin`
```
	<pluginManagement>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<configuration>
					<source>1.6</source>
					<target>1.6</target>
				</configuration>
			</plugin>
		</plugins>
	</pluginManagement>
```

## Update Project and Fix Build Path

- Maven - update project
- Configure build path -> Add missing source folders - src/main/java, src/test/java
- Configure build path -> Update JRE library

## Add new Server

- Add Wildfly 12 as new Server
- add jboss-web.xml under `WEB-INF` (deployment descriptor for wildfly)


## Create new package and classes

- com.infinispan.tutorial
- add new classes under this package - `App.java` and `Cache.java`
- Run as -> Run on Server -> Select Wildfly 12
- from browser, hit http://localhost:8080/main and the message from the Cache should be displayed
