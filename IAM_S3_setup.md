# Setup S3 bucket, IAM user and IAM Role for node discovery

## S3 bucket

An S3 bucket is a simple storage service provided by AWS. 
We will use S3 to store files which will be shared amongst all the nodes to help them discover each other.

- Go to console -> S3
- Create a new bucket (I created `s3-ping-infinispan`, you can choose another unique name)
- Keep default settings and save (a new bucket will be created)

### Add policy to access bucket data

In AWS, permissions to access resources is created using policy documents. 
A policy document is a JSON, containing information about access permission of different AWS resources.
We will create a new policy to provide permission to list all buckets and read/write from `s3-ping-infinispan`.

- Go to console -> IAM -> Policies
- Create a new policy
- Add the following JSON

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "s3:GetBucketLocation",
                "s3:ListAllMyBuckets"
            ],
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "s3:ListBucket"
            ],
            "Resource": [
                "[ARN_BUCKET]"
            ]
        },
        {
            "Effect": "Allow",
            "Action": [
                "s3:PutObject",
                "s3:GetObject"
            ],
            "Resource": [
                "[ARN_BUCKET]/*"
            ]
        }
    ]
}
```
- replace the [ARN_BUCKET] with the ARN of your bucket.
- Give name (`NATIVE_S3_PING`) and description to the policy and save it.

## IAM User (local testing)

When performing the local testing, An IAM user is required to be created and provided accessed to the `s3-ping-infinispan` which will facilitate the node discovery.

- Go to AWS console -> IAM
- Create a new user with Programmatic access (with an access key) (I created `jgroups-user`, you can select any name you wish)
- Do not add any permissions yet
- Save the new user and download the csv containing user details (secret, access key, arn)

### Attach the policy to your user

We will attach the permission policy we created to this user to provide it with appropriate access to `s3-ping-infinispan`

- Go to users and select the IAM user you already created
- Go to Permissions and select `Add from existing policies`
- Select the policy you created (`NATIVE_S3_PING`)
- Save

This will provide permissions to your IAM user to have access to the S3 bucket you created.

### Add IAM credentials to `~/.aws/credentials`

We need to provide IAM user credentials to AWS SDK using `~/.aws/credentails` file

- Create the file `~/.aws/credentails`
- Add the following

```
[default]
aws_access_key_id = <IAM_KEY>
aws_secret_access_key = <IAM_SECRET>
```

- Replace the <IAM_KEY> and <IAM_SECRET> with your IAM user's details.
- When then application runs locally now, AWS SDK will get the credentails from this file during the runtime

## IAM Role (testing on EC2)

When deploying in AWS, we need to provide the SDK the IAM credentials using an IAM role instead of the `~/.aws/credentails`.

- Create a new Role -> AWS Service -> EC2
- Select the policy you created (`NATIVE_S3_PING` in my case) under permission policy
- Provide a name - `TRUST_EC2_IAM` (Could be anything you like)
- Save the role
- Go to EC2 instance running the application
- Actions -> Instance Settings -> Attach/Replace IAM Role
- Attach the Role created and save/apply

These details (S3 bucket, IAM user and IAM Role) can now be used to setup the node discovery protocol for the jgroups configuration.