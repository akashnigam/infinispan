package com.infinispan.tutorial;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.infinispan.Cache;
import org.infinispan.manager.EmbeddedCacheManager;

import com.infinispan.tutorial.config.CacheEventsListerner;
import com.infinispan.tutorial.config.CacheManagerEventsListerner;

@ApplicationScoped
public class MessageService {

	private static final String MESSAGE_CACHE = "MESSAGE_CACHE";
	private static final String CACHE_KEY = "MSG_KEY";

	private EmbeddedCacheManager cacheManager;

	private Cache<String, List<Message>> cache;

	private MessageDao messageDao;

	public MessageService() {
		// no-args constructor for CDI
	}

	@Inject
	public MessageService(MessageDao messageDao, EmbeddedCacheManager cacheManager) {
		this.messageDao = messageDao;
		this.cacheManager = cacheManager;
		this.cacheManager.addListener(new CacheManagerEventsListerner());
		this.cache = cacheManager.getCache(MESSAGE_CACHE);
		this.cache.addListener(new CacheEventsListerner());
	}

	public List<Message> getMessages() {
		List<Message> messages = cache.get(CACHE_KEY);
		if (messages == null) {
			messages = messageDao.getMessages();
			cache.put(CACHE_KEY, messages);
		}
		return messages;
	}

	public void resetCache() {
		cache.remove(CACHE_KEY);
	}
}
