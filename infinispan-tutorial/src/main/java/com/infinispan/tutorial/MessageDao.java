package com.infinispan.tutorial;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class MessageDao {

	private final int messageCount = 20;

	public List<Message> getMessages() {
		List<Message> messages = new ArrayList<Message>();
		for (int i = 0; i < messageCount; i++) {
			messages.add(new Message(i, "Message" + i));
		}
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return messages;
	}
}
