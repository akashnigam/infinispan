package com.infinispan.tutorial.config;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;

import org.infinispan.configuration.cache.CacheMode;
import org.infinispan.configuration.cache.Configuration;
import org.infinispan.configuration.cache.ConfigurationBuilder;
import org.infinispan.configuration.global.GlobalConfiguration;
import org.infinispan.configuration.global.GlobalConfigurationBuilder;
import org.infinispan.manager.DefaultCacheManager;
import org.infinispan.manager.EmbeddedCacheManager;

@ApplicationScoped
public class CacheManagerConfig {

	@Produces
	public EmbeddedCacheManager clusteredCacheManager() {
		GlobalConfiguration globalConfiguration = new GlobalConfigurationBuilder().clusteredDefault().transport()
				.clusterName("message-cluster")
				//.addProperty("configurationFile", "jgroups-ec2.xml")
				.build();
		Configuration configuration = new ConfigurationBuilder().clustering().cacheMode(CacheMode.DIST_SYNC).memory()
				.size(5).build();
		return new DefaultCacheManager(globalConfiguration, configuration);
	}
}
