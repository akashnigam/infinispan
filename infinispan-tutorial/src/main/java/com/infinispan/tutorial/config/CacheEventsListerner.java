package com.infinispan.tutorial.config;

import java.util.List;

import org.infinispan.notifications.Listener;
import org.infinispan.notifications.cachelistener.annotation.CacheEntryCreated;
import org.infinispan.notifications.cachelistener.annotation.CacheEntryRemoved;
import org.infinispan.notifications.cachelistener.event.CacheEntryCreatedEvent;
import org.infinispan.notifications.cachelistener.event.CacheEntryRemovedEvent;

import com.infinispan.tutorial.Message;

@Listener
public class CacheEventsListerner {

	@CacheEntryCreated
	public void logDetails(CacheEntryCreatedEvent<String, List<Message>> event) {
		System.out.println("****** Cache entry added ******");
		System.out.println(event.getType());
		System.out.println("*******************************");
	}

	@CacheEntryRemoved
	public void logDetails(CacheEntryRemovedEvent<String, List<Message>> event) {
		System.out.println("****** Cache entry removed ******");
		System.out.println(event.getType());
		System.out.println("*********************************");
	}
}
