package com.infinispan.tutorial.config;

import org.infinispan.notifications.Listener;
import org.infinispan.notifications.cachemanagerlistener.annotation.CacheStarted;
import org.infinispan.notifications.cachemanagerlistener.annotation.CacheStopped;
import org.infinispan.notifications.cachemanagerlistener.event.CacheStartedEvent;
import org.infinispan.notifications.cachemanagerlistener.event.CacheStoppedEvent;

@Listener
public class CacheManagerEventsListerner {

	@CacheStarted
	public void logDetails(CacheStartedEvent event) {
		System.out.println("****** Cache manager - start event ******");
		System.out.println(event.getType());
		System.out.println("*********************************");
	}

	@CacheStopped
	public void logDetails(CacheStoppedEvent event) {
		System.out.println("****** Cache manager - stop event ******");
		System.out.println(event.getType());
		System.out.println("*********************************");
	}
}
