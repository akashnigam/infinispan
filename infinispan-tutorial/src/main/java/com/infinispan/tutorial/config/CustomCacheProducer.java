package com.infinispan.tutorial.config;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;

import org.infinispan.cdi.embedded.ConfigureCache;
import org.infinispan.configuration.cache.Configuration;
import org.infinispan.configuration.cache.ConfigurationBuilder;

@ApplicationScoped
public class CustomCacheProducer {

	@ConfigureCache("message-cache")
	@MessageCache
	@Produces
	public Configuration messageCacheConfiguration() {
		ConfigurationBuilder builder = new ConfigurationBuilder();
		builder.memory().size(1000);
		return builder.build();
	}

}
