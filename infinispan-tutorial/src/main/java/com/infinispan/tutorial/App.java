package com.infinispan.tutorial;

import java.io.IOException;
import java.io.PrintWriter;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = { "/main" })
public class App extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Inject
	private MessageService messageService;

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		response.setContentType("text/html");
		PrintWriter writer = response.getWriter();
		for (Message message : messageService.getMessages()) {
			writer.println(message.getText());
			writer.println("<br>");
		}
		writer.print(addResetButton());
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		messageService.resetCache();
	}

	private String addResetButton() {
		String btn = "<button type='button' onclick='reset()' style='padding: 2%;'>Reset Cache</button>";
		btn += addScript();
		return btn;
	}
	
	private String addScript() {
		String script = "<script>" + 
						"function reset() {" + 
						"  var xhttp = new XMLHttpRequest();" + 
						"  xhttp.open('POST', '/main', true);" + 
						"  xhttp.send();" + 
						"}" + 
						"</script>";
		return script;
	}
}
