# Infinispan

Infinispan is an *In-memory, key-value data store*, that could be used as an embedded library with your Java application or accessed remotely.
Infinispan can be used as a local, clustered, remote cache and as a data grid.
It can integrate with several frameworks like Hibernate (L2 cache), Hadoop, Spring Cache etc.

Infinispan comes in 2 variants for caching:
	- *Embedded Caching* - in memory clustered and non-clustered caching, i.e., without using external infinispan server
	- *Remote Caching* - dedicated caching server, accessed through REST, Websockets etc


In this tutorial we will learn about how to use Infinispan as a local and clustered cache with a java application in embedded mode.


## Default Cache

- The Default cache is provided out of the box by infinispan having a pre-defined default configuration to be used in-memory
- Default cache can be used by simply injecting the `org.infinispan.Cache` into the code.

```
	@Inject
	private Cache<String, List<Message>> cache;
```
- The default configuration for this cache can be modified by specifying a method annotated as a `@Produces`

```
	@Produces
    public Configuration defaultEmbeddedConfiguration () {
        return new ConfigurationBuilder()
                         .eviction()
                                 .strategy(EvictionStrategy.LRU)
                                 .maxEntries(100)
                         .build();
    }
```
- This method is used to load the configuration of the default cache at the runtime.

## Custom Cache

- A custom cache can be defined by first, defining a qualifier for the cache (a custom annotation)

```
	@Qualifier
	@Target({ ElementType.FIELD, ElementType.PARAMETER, ElementType.METHOD })
	@Retention(RetentionPolicy.RUNTIME)
	@Documented
	public @interface MessageCache {}
```
- Then defining a configuration for this qualifier

```
	@ConfigureCache("message-cache")
	@MessageCache
	@Produces
	public Configuration messageCacheConfiguration() {
		ConfigurationBuilder builder = new ConfigurationBuilder();
		builder.memory().size(1000);
		return builder.build();
	}
```
- And then simply injecting the `org.infinispan.Cache` and annotating with the defined qualifier (@MessageCache)

```
	@Inject @MessageCache
	private Cache<String, List<Message>> cache;
```
- Using above customization will load this specific `message-cache` instead of the default cache at the runtime.


## Cache Manager

- A cache manager acts as a container for caches and exposes various operations related to container to provide improved flexibility for harnessing the full power of infinispan.
- A cache manager provides 
	- Clustered caches
	- Cache interface to retrieve actual cache instance and perform cache level operations
	- Listeners and notifications for cache level(entry added/removed) and cache manager level(node joined/removed from cluster)
	- Asynchronous non-blocking API
	- Invocation flags (per cache call customizations)

- Infinispan provides a `EmbeddedCacheManager` for embedded caching and `RemoteCacheManager` for remote caching.
- Inject these default cache managers in code and start using them like:

```
	@Inject
	EmbeddedCacheManager cacheManager;
```
or
```
	@Inject
	RemoteCacheManager cacheManager;
```

### Creating a *Non-clustered Cache* using `EmbeddedCacheManager`

- Create the configuration for the EmbeddedCacheManager for non-clustered cache

```
	@Produces
    @ApplicationScoped
    public EmbeddedCacheManager defaultEmbeddedCacheManager() {
        Configuration cfg = new ConfigurationBuilder()
	            .eviction()
	            .strategy(EvictionStrategy.LRU)
	            .maxEntries(150)
	            .build();
        return new DefaultCacheManager(cfg);
    }
```
- Inject the EmbeddedCacheManager directly in the service and get the instance of the cache from it to use in non-clustered mode

```
	@Inject
	EmbeddedCacheManager cacheManager;
```

### Creating a *Clustered* cache using `EmbeddedCacheManager`

Infinispan can be used as local cache mode or as a clustered cache.
Setting up the `GlobalConfiguration` enables infinspan's cache manager to start working in the clustered mode.
In a clustered mode, nodes are able to discover each other, setup the topology, manage topology and share cache data between them.

- Create the configuration for the EmbeddedCacheManager for clustered cache - global configuration

```
	@Produces
	@ApplicationScoped
	public EmbeddedCacheManager defaultClusteredCacheManager() {
		GlobalConfiguration globalConfiguration = new GlobalConfigurationBuilder().clusteredDefault().transport()
				.clusterName("message-cluster")
				.build();
		Configuration configuration = new ConfigurationBuilder().clustering().cacheMode(CacheMode.DIST_SYNC).memory()
				.size(5).build();
		return new DefaultCacheManager(globalConfiguration, configuration);
	}
```

- Inject the EmbeddedCacheManager directly in the service and get the instance of the cache from it to use in clustered mode

```
	@Inject
	EmbeddedCacheManager cacheManager;
```

- Multiple instance of the applications on same network will be able to discover each other, will form the cache cluster and work synchronously.
- For example, on your local machine, start 2 instances of this application
- Notice how the logs of the first instance display information about the cluster being added with another node, when the second instance is up and application is loaded

### Jgroups

In a clustered mode, cache managers use JGroups' discovery protocols to automatically discover neighboring instances on the same local network and form a cluster.
This jgroups' configuration is provided out-of-the-box for node discovery and transport protocols for the cluster formation and communication.
However, we would need to customize these when we want to deploy our application in a cloud service example AWS or Azure. 

Jgroups is a toolkit for reliable group communication. It provides set of protocols to be used for communication between different `nodes` in a `cluster`.

Using Jgroups for infinispan includes configuring

1) Transport protocol - how data is sent across different nodes
2) Discovery protocol - how members discover each other

UDP (transport protocol) along with PING (discovery protocol) is most widely used combination for this purpose. And for local development, it can be used easily.
However, most cloud service providers do not allow IP multicasting (which is used for discovery (PING) and transport (UDP)) since it could cause congestion in their network.
Jgroups supports many other protocols for such purpose and therefore, while deploying the application to any such cloud providers, an alternate protocol can be used depending upon the provider.

For example, if AWS is used, a TCP (transport protocol) can be used along with `NATIVE_S3_PING` (discovery protocol), 
or if Microsoft Azure is used, TCP can be used with `AZURE_PING` etc.

`NATIVE_S3_PING` for example makes use of a S3 bucket to store necessary files for cluster formation, accessible by fellow nodes to discover each other and create the cluster.

In our case, we will be deploying the application to AWS, so we will be using TCP along with NATIVE_S3_PING.

> Use `NATIVE_S3_PING` instead of `S3_PING`, because S3_PING uses old and deprecated way to authenticate (HTTP), whereas NATIVE_S3_PING uses AWS SDK provided means to authenticate with AWS S3 bucket, which is more secured and reliable. Using `S3_PING` might cause problems, so it is better to use `NATIVE_S3_PING`.

### Node discovery configuration

> Since we will be making use of S3 bucket for node discovery, follow the documentation [IAM_S3_setup.md](IAM_S3_setup.md) to setup S3 Bucket, IAM user and IAM Role before continuing with the tutorial

### Configuring jgroups - `jgroups-ec2.xml`

After setting up the S3 bucket, IAM user and IAM Role, create a file under `src/main/resources` - `jgroups-ec2.xml` with following information :

```
<config xmlns="urn:org:jgroups"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="urn:org:jgroups http://www.jgroups.org/schema/jgroups-4.0.xsd">
	
	<TCP bind_addr="${jgroups.tcp.address:127.0.0.1}"
		bind_port="${jgroups.tcp.port:7800}" enable_diagnostics="false"
		thread_naming_pattern="pl" send_buf_size="640k"
		sock_conn_timeout="300" bundler_type="no-bundler"

		thread_pool.min_threads="${jgroups.thread_pool.min_threads:0}"
		thread_pool.max_threads="${jgroups.thread_pool.max_threads:200}"
		thread_pool.keep_alive_time="60000" />

	<org.jgroups.aws.s3.NATIVE_S3_PING
		region_name="ap-south-1"
		bucket_name="s3-ping-infinispan" 
		bucket_prefix="jgroups" />

	<MERGE3 min_interval="10000" max_interval="30000" />

	<FD_SOCK />

	<FD_ALL timeout="60000" interval="15000"
		timeout_check_interval="5000" />

	<VERIFY_SUSPECT timeout="5000" />

	<pbcast.NAKACK2 use_mcast_xmit="false"
		xmit_interval="100" xmit_table_num_rows="50"
		xmit_table_msgs_per_row="1024" xmit_table_max_compaction_time="30000"
		resend_last_seqno="true" />

	<UNICAST3 xmit_interval="100" xmit_table_num_rows="50"
		xmit_table_msgs_per_row="1024" xmit_table_max_compaction_time="30000"
		conn_expiry_timeout="0" />

	<pbcast.STABLE stability_delay="500"
		desired_avg_gossip="5000" max_bytes="1M" />

	<pbcast.GMS print_local_addr="false"
		install_view_locally_first="true"
		join_timeout="${jgroups.join_timeout:5000}" />

	<MFC max_credits="2m" min_threshold="0.40" />

	<FRAG3 />

</config>

```

Change the `region_name`, `bucket_name` and `bucket_prefix` with the ones applicable to your case.

Some other useful features of infinispan cache:

### Listeners & Notifications

Clients can register for events in Cache Manager and individual caches and can be notified when the event occurs.

- By making use of `@Listener` annotation, we can mark a class as a cache manager event listener or a cache event listener.
- Cache manager level notifications and events can be handled by using `@CacheStarted`, `@CacheStopped` annotations.
- Cache level notifications and events can be handled by using `@CacheEntryCreated`, `@CacheEntryModified` & `@CacheEntryRemoved` annotations.
- These listeners need to be attached to the Cache Manager or the Cache in order for them to be active.

Example :

```
@Listener
public class CacheManagerEventsListerner {

	@CacheStarted
	public void logDetails(CacheStartedEvent event) {
		System.out.println("****** Cache manager - start event ******");
		System.out.println(event.getType());
	}
}
```

or

```
@Listener
public class CacheEventsListerner {

	@CacheEntryCreated
	public void logDetails(CacheEntryCreatedEvent<String, List<Message>> event) {
		System.out.println("****** Cache entry added ******");
		System.out.println(event.getType());
	}
}
```

- Attach these listeners as follows

```
cacheManager.addListener(new CacheManagerEventsListerner());

// or

cache.addListener(new CacheEventsListerner());
```

### Silent Read

`putForExternalRead()` is used to read cache data with the advantage of avoiding complete operation failure when something goes wrong while reading data from cache.
It fails silently, without throwing exception in the main thread and returns to the caller promptly.


### Asynchronous API

In addition to `Cache.put()` and `Cache.remove()` methods, which are synchronous and blocking in nature, infinispan provides `Cache.putAsync()` and `Cache.removeAsync()` which are asynchronous methods in nature, do not block the thread while performing cache level updates and return *Futures* to process on the information later.


### Invocation Flags

Invocation flags can be used to customize a cache behaviour.
`putForExternalRead()` and the asynchronous API for example make use of these Invocation Flags internally to provide their customized behaviours.

Examples of Invocation Flags - `ZERO_LOCK_ACQUISITION_TIMEOUT`, `CACHE_MODE_LOCAL`, `SKIP_LOCKING` etc. (checkout `org.infinispan.context.Flag` enum).

Usage : 

```
AdvancedCache advancedCache = cache.getAdvancedCache();
advancedCache.withFlags(Flag.CACHE_MODE_LOCAL, Flag.SKIP_LOCKING)
   .withFlags(Flag.FORCE_SYNCHRONOUS)
   .put("hello", "world");
```

### Clustering modes

Individual caches can then be configured in 4 different modes:

1) **Local Mode**: Cache data is never shared among nodes in a cluster and always kept local.
2) **Replicated Mode**: Data changes are replicated to all nodes in a cluster. So that every node contains the fresh copy of the data at any time.
3) **Invalidation Mode**: When data changes in one node, an invalidation message is sent to other nodes to remove that data from their local cache.
4) **Distributed Mode**: Data is replicated to a fixed number of nodes. Helpful in scaling the cluster horizontally.

